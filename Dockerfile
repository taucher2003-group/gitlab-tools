FROM ruby:3.2.2
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

COPY bin/ bin/
COPY lib/ lib/
