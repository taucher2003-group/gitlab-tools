# frozen_string_literal: true

module Gitlab
  ACCESS_LEVELS = {
    10 => 'Guest',
    20 => 'Reporter',
    30 => 'Developer',
    40 => 'Maintainer',
    50 => 'Owner',
  }.freeze

  class Client
    module Groups
      def all_group_members(group, options = {})
        get("/groups/#{url_encode group}/members/all", query: options)
      end
    end

    module AccessTokens
      def project_access_tokens(project)
        get("/projects/#{url_encode project}/access_tokens")
      end

      def group_access_tokens(group)
        get("/groups/#{url_encode group}/access_tokens")
      end
    end

    include AccessTokens
  end
end
