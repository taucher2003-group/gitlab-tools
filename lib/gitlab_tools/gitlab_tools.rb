# frozen_string_literal: true

module GitlabTools
  CommandError = Class.new(StandardError)

  # All commands that have been registered and should thus be available to the
  # user.
  def self.commands
    @commands ||= {}
  end

  # Runs a chatops command.
  #
  # argv - The commandline arguments that were used to invoke the program.
  def self.run(argv = [])
    @name = argv.shift

    @chat_input = argv.join(' ')

    command_class = commands[@name]

    raise CommandError, "The command #{@name.inspect} does not exist" unless command_class

    split_chat_input = split_input(@chat_input)

    command_class.perform(split_chat_input)
  end

  def self.split_input(string)
    # macOS "smart quotes" can interfere with option parsing, so dumb them down
    string = string
             .tr('“”«»', '"')
             .tr('‘’‹›', "'")
             .gsub(/[[:blank:]]/, ' ') # replace non-breaking space otherwise it won't split the words

    Shellwords.split(string)
  end
end
