# frozen_string_literal: true

module GitlabTools
  module GitlabClient
    def gitlab_token
      GitlabTools::Helper::Env.fetch('GITLAB_API_TOKEN', default: '')
    end

    def gitlab_client
      Gitlab.client(endpoint: GitlabTools::Helper::Env.fetch('GITLAB_API_URL', 'CI_API_V4_URL'),
                    private_token: gitlab_token)
    end
  end
end
