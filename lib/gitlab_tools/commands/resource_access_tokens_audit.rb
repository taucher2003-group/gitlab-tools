# frozen_string_literal: true

module GitlabTools
  module Commands
    class ResourceAccessTokensAudit
      include Command
      include GitlabClient

      usage "#{command_name} [GROUP]"
      description 'Create an audit of all resource access tokens'

      def perform
        group_name = arguments.first
        root_group = gitlab_client.group(group_name)

        projects = all_projects(group_name)
        groups = [root_group] + all_groups(group_name)

        result = []

        groups.each do |group|
          tokens = group_access_tokens(group)
          next if tokens.empty?

          result << ''
          result << "Group: `#{group.full_path}`"

          tokens.each do |token|
            result << "- #{token.id}, #{token.name}, #{Gitlab::ACCESS_LEVELS[token.access_level]}, #{token.scopes}, " \
                      "expires on #{token.expires_at}"
          end
        end

        projects.each do |project|
          tokens = project_access_tokens(project)
          next if tokens.empty?

          result << ''
          result << "Project: `#{project.path_with_namespace}`"

          tokens.each do |token|
            result << "- #{token.id}, #{token.name}, #{Gitlab::ACCESS_LEVELS[token.access_level]}, #{token.scopes}, " \
                      "expires on #{token.expires_at}"
          end
        end

        result.join("\n")
      end

      private

      def all_projects(group)
        gitlab_client.group_projects(group, include_subgroups: true, with_shared: false).auto_paginate
      end

      def all_groups(group)
        gitlab_client.group_descendants(group, all_available: true).auto_paginate
      end

      def project_access_tokens(project)
        gitlab_client.project_access_tokens(project.id).auto_paginate
      end

      def group_access_tokens(group)
        gitlab_client.group_access_tokens(group.id).auto_paginate
      end

      def process_token(token)
        token.name
      end
    end
  end
end
