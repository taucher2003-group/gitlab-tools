# frozen_string_literal: true

module GitlabTools
  module Commands
    class MemberAudit
      include Command
      include GitlabClient

      usage "#{command_name} [GROUP]"
      description 'Create an audit of all memberships'

      options do |o|
        o.bool '--all', 'Include inherited members and members from group shares in the report'
      end

      def perform
        group_name = arguments.first
        group = gitlab_client.group(group_name)
        projects = all_projects(group_name)
        subgroups = all_groups(group_name)

        result = []
        result << "Group: `#{group.full_path}`"
        result << format_members(group_members(group.id))

        subgroups.sort_by(&:full_path).each do |subgroup|
          result << ''
          result << "Group: `#{subgroup.full_path}`"
          result << format_members(group_members(subgroup.id))
        end

        projects.sort_by(&:path_with_namespace).each do |project|
          result << ''
          result << "Project: `#{project.path_with_namespace}` #{project.archived ? '(archived)' : ''}"
          result << format_members(project_members(project.id))
        end

        result.join("\n")
      end

      private

      def all_projects(group)
        gitlab_client.group_projects(group, include_subgroups: true, with_shared: false).auto_paginate
      end

      def all_groups(group)
        gitlab_client.group_descendants(group, all_available: true).auto_paginate
      end

      def group_members(group)
        members = options[:all] ? gitlab_client.all_group_members(group) : gitlab_client.group_members(group)

        members.auto_paginate
      end

      def project_members(project)
        members = options[:all] ? gitlab_client.all_members(project) : gitlab_client.team_members(project)

        members.auto_paginate
      end

      def format_members(members)
        if members.empty?
          'none'
        else
          members.sort_by(&:access_level).reverse.map do |member|
            "- #{member.username} (#{Gitlab::ACCESS_LEVELS[member.access_level]})"
          end
        end
      end
    end
  end
end
